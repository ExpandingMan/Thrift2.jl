```@meta
CurrentModule = Thrift2
```

# API

```@index
```

## Main
```@docs
CompactProtocol
@thriftstruct

juliatype
thrifttype
ThriftType
```

## Internals
```@docs
encodetype
decodetype
thriftfieldtype

zigzag
unzigzag

writevarint
readvarint

readfield
skipfield
skipstruct
StructReadState
```
