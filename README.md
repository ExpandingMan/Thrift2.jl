# Thrift2

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/Thrift2.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/Thrift2.jl/main?style=for-the-badge)](https://gitlab.com/ExpandingMan/Thrift2.jl/-/pipelines)

A Julia implementation of [Apache Thrift](https://thrift.apache.org/), based on [this
specification](https://erikvanoosten.github.io/thrift-missing-specification/).

